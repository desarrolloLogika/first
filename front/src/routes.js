import React from 'react'
import { Switch, Route } from 'react-router-dom'

import App from './App.js'
import Test from './components/Test.js'

export const RoutesApp = (
    <Switch>
      <Route path="/" exact component={App} />
      <Route path="/test" exact component={Test} />
    </Switch>
)
